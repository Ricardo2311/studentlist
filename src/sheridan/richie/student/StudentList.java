package sheridan.richie.student;

public class StudentList {

    public static void main(String[] args) {

        String name = args[0];
        String id = args[1];

        Student student = new Student(name, id);

        System.out.println("Printing student:");

        String format = "The student's name is %s  and their id is %s \n ";

        System.out.printf(format, student.getName(), student.getId());
    }

}
