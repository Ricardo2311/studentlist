package sheridan.richie.student;


public class Student {
    
    private String name;
    private String id;
    private String program;
    
    public void setId(String id) {
        this.id = id;
    }

    public void setProgram(String program) {
        this.program = program;
    }
   
    
    public String getProgram() {
        return program;
    }
    
   
    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

    public String getId() {
        return id;
    }

    public Student(String name, String id, String program) {
        this.name = name;
        this.id = id;
        this.program = program;
    }

    

}

