package sheridan.richie.arrays;

import java.util.Arrays;

/**
 * * * @author Richi Van Juárez
 */
public class SortNumbers {

    public static void sortNumbers(int[] numbers) {
        System.out.println("Sorted" + Arrays.toString(numbers));
        Arrays.sort(numbers);
        
        System.out.println("Sorted" + Arrays.toString(numbers));
    }

    public static void main(String[] args) {
        int numbers[] = {1456,1458,1254,1472,1457};
        sortNumbers(numbers);
    }
}
    