
package sheridan.richie.arrays;

import java.util.Scanner;

/**
 *
 * @author Richi Van Juárez
 */
public class ReverseString {

    
    public static void main (String[] args) {
      
        Scanner keyboard = new Scanner(System.in);
        String word="";
        int i;
        char[] chr;
        System.out.println("Enter a Word");
        word= keyboard.nextLine();
        chr= word.toCharArray();
        for (i=word.length()-1; i>=0; i--)
        System.out.println("the word in reverse is"+(chr[i])+".");
        
    }
    
}
