package sheridan.richie.arrays;

import java.util.Arrays;

/**
 * * * @author Richi Van Juárez
 */
public class SortWords {

    public static void sortWords(String[] array) {
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        String[] array = {"Java","Python","PHP","C#","C Programming","C++"};
        sortWords(array);
    }
}
